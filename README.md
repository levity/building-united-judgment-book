
# Table of Contents

1.  [Introduction](#org9aa469b)
    1.  [The format](#org892a01b)
    2.  [Contents](#orge3653d4)
        1.  [The "original" PDF](#org024b26a)
        2.  [The OCR output text](#org5e596ee)
        3.  [The images/ directory](#org4739890)
        4.  [README.org](#orgba47d20)
        5.  [The org file](#orgd791caf)


<a id="org9aa469b"></a>

# Introduction

This is a machine-readable version of the book Building United Judgment: A Handbook for Consensus, a copy of which should be included here. See [1.2](#orge3653d4) below for more information.


<a id="org892a01b"></a>

## The format

This file is in Org format, a type of markup that is understood by Emacs and some other editor software. It can be used to generate HTML and other formats.


<a id="orge3653d4"></a>

## Contents

The file is based on a scan of the PDF document which underwent Optical Character Recognition (OCR). The OCR'd file was then edited "by hand" in Emacs org-mode. The directory in which this file exists should contain the following:


<a id="org024b26a"></a>

### The "original" PDF

This is the PDF version of the scanned book as found on the Internet Archive at <https://archive.org/details/BuildingUnitedJudgmentAHandbookForConsensusDecisionMaking>


<a id="org5e596ee"></a>

### The OCR output text

This is the text version of the PDF, obtained at the same url


<a id="org4739890"></a>

### The images/ directory

This contains images from the PDF, named for the chapter and page number in which they appear in the original text.


<a id="orgba47d20"></a>

### README.org

This file, also in Org format (though you might be reading an HTML or Markdown file exported from it)


<a id="orgd791caf"></a>

### The org file

If your editor can display Org format properly, this file is all you need to read (or edit) the book. Otherwise you'll need to export it to HTML (recommended) or one of the other formats Org supports (such as Markdown or LaTEX).

HTML is recommended because the images may not display properly or their attributes (captions, alt-text, etc) may not work correctly in other formats. Other parts of the book should still work correctly in all formats, but no guarantees. 

